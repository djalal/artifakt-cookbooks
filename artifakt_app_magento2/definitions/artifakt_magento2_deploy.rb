#
# Cookbook Name: artifakt_app_magento2
# Definition: artifakt_magento2_deploy
#

define :artifakt_magento2_deploy  do
  release_path = node[:app_release_path]

  display_text do
    text "Deploy Magento 2 for App type: #{node[:app][:type]} / App version: #{node[:app][:version]}"
  end

  execute 'Permission bin/magento' do
    cwd "#{release_path}/"
    command "chmod u+x bin/magento"
  end

  execute "Remove env.php is not installed" do
    cwd "#{release_path}/"
    command "rm -f app/etc/env.php"
    only_if { node[:app][:is_installed] == 'false' }
  end

  if node[:app][:is_installed] == 'true'

    if !File.exist?("#{release_path}/app/etc/config.php")
      display_text do
        text "No app/etc/config.php file found"
      end
      display_command do
        command "php bin/magento module:enable --all"
      end
      run_command do
        command 'php bin/magento module:enable --all'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    display_command do
      command "php bin/magento maintenance:enable"
    end
    run_command do
      command 'php bin/magento maintenance:enable'
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

    if node[:stack][:mode] == 'prod'
      display_command do
        command "php bin/magento deploy:mode:set production -s"
      end
      run_command do
        command 'php bin/magento deploy:mode:set production -s'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    if node[:stack][:mode] == 'dev'
      display_command do
        command "php bin/magento deploy:mode:set developer -s"
      end
      run_command do
        command 'php bin/magento deploy:mode:set developer -s'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end


    if node[:opsworks][:main_instance]
      if node[:opsworks][:instance][:hostname] == node[:opsworks][:main_instance]
        display_command do
          command "php bin/magento setup:upgrade"
        end
        run_command do
          command 'php bin/magento setup:upgrade'
          user node[:app_user]
          group node[:app_group]
          environment node[:deploy][node[:app_name]][:environment_variables]
          cwd "#{release_path}/"
        end
      end
    else
      display_command do
        command "php bin/magento setup:upgrade"
      end
      run_command do
        command 'php bin/magento setup:upgrade'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    if node[:stack][:mode] == 'prod'
      display_command do
        command "php bin/magento setup:di:compile"
      end
      run_command do
        command 'php bin/magento setup:di:compile'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    if node[:stack][:mode] == 'prod'
      display_command do
        command "php bin/magento setup:static-content:deploy en_US fr_FR"
      end
      run_command do
        command 'php bin/magento setup:static-content:deploy en_US fr_FR'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    if node[:command][:type] != 'setup'
      display_command do
        command "php bin/magento cache:flush"
      end
      run_command do
        command 'php bin/magento cache:flush'
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    display_command do
      command "php bin/magento maintenance:disable"
    end
    run_command do
      command 'php bin/magento maintenance:disable'
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

  end

end

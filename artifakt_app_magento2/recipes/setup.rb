#
# Cookbook Name: artifakt_app_magento2
# Recipe: setup
#
if node[:es][:installed]
  include_recipe 'artifakt_elasticsearch::setup'
end

if (node[:app][:type] != 'magento2' && node[:app][:version] != '2.4')
  bash "Install N98" do
    user "root"
    code <<-EOH
        wget https://files.magerun.net/n98-magerun2.phar
        mv n98-magerun2.phar /usr/local/bin/n98-magerun2
        chmod +x /usr/local/bin/n98-magerun2
    EOH
  end
end


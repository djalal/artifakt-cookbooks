#
# Cookbook Name: artifakt_app_magento2
# Recipe: maintenance-on
#

node[:deploy].each do |app_name, deploy|
  bash "maintenance_on" do
    user "root"
    cwd "#{node[:deploy][node[:app_name]][:absolute_code_root]}/"
    code <<-EOH
      php bin/magento maintenance:enable
    EOH
  end
end

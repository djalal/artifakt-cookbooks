#
# Cookbook Name: artifakt_app_akeneo
# Recipe: setup
#

bash "install nodejs" do
    user "root"
    code <<-EOH
    curl -sL https://rpm.nodesource.com/setup_10.x | bash -
    yum -y install nodejs
    npm install yarn -g
    EOH
end

bash "install imageMagick" do
    user "root"
    code <<-EOH
    yum -y install php7-pear ImageMagick ImageMagick-devel ImageMagick-perl
    printf "\n" | pecl7 install imagick
    echo extension=imagick.so >> /etc/php.ini
    service httpd restart
    EOH
end

template "/etc/yum.repos.d/elasticsearch.repo" do
    source "elasticsearch.repo.erb"
    cookbook "artifakt_app_akeneo"
    owner 'root'
end

bash "install elasticsearch" do
    user "root"
    code <<-EOH
    yum -y remove java
    yum -y install java-1.8.0-openjdk-devel
    rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
    yum -y install elasticsearch
    chmod g+w /etc/elasticsearch
    service elasticsearch start
    EOH
end

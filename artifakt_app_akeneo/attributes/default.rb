#
# Cookbook Name: artifakt_app_akeneo
# Attributes: default
#

default[:akeneo][:document_root] = 'web'
default[:akeneo][:shared_directories] = [ 'web/uploads','app/uploads','app/archive','app/import','app/export','web/import','web/export','app/file_storage','app/logs' ]
default[:akeneo][:cached_directories] = [ 'app/cache' ]
default[:akeneo][:media_directories] = []
default[:akeneo][:log_directories] = []
default[:akeneo][:session_directories] = []
default[:akeneo][:config_file] = {'parameters.yml.erb' => 'app/config/parameters.yml'}
default[:akeneo][:files_to_stream] = ['app/logs/prod.log', 'app/logs/dev.log']

if node[:app][:version].slice(0..0) == '4'
    default[:akeneo][:version_es] = 7
else
    default[:akeneo][:version_es] = 6
end
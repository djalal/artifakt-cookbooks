#
# Cookbook Name: artifakt_app_akeneo
# Definition: artifakt_akeneo_deploy
#

define :artifakt_akeneo_deploy  do
  release_path = node[:app_release_path]
  env = "--env=#{node[:stack][:mode]}"

  if node[:app][:version].slice(0..0) == '3'

    display_command do
      command "yarn install"
    end
    run_command do
      command 'yarn install'
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

    display_command do
      command "php bin/console cache:clear --no-warmup #{env}"
    end
    run_command do
      command "php bin/console cache:clear --no-warmup #{env} --ansi"
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

    display_command do
      command "php bin/console pim:installer:assets --symlink #{env}"
    end
    run_command do
      command "php bin/console pim:installer:assets --symlink #{env} --ansi"
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

    if node[:app][:is_installed] == 'false'
      # Be very careful about this command line! It must be run once, in the first setup
      # This command, drop the databse, and recreate a new one
      display_command do
        command "php bin/console pim:install --force --symlink #{env}"
      end
      run_command do
        command "php bin/console pim:install --force --symlink #{env} --ansi"
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end
    end

    display_command do
      command "yarn run webpack"
    end
    run_command do
      command 'yarn run webpack'
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

  else
    if node[:app][:is_installed] == 'true'
      execute "Dump assetic" do
          user 'root'
          cwd "#{release_path}/"
          command "php app/console assetic:dump #{env}"
      end

      execute "Dump Oro assetic" do
          user node[:app_user]
          group node[:app_group]
          cwd "#{release_path}/"
          command "php app/console oro:assetic:dump #{env}"
      end

      bash "Deploy data" do
          user node[:app_user]
          group node[:app_group]
          cwd "#{release_path}/"
          code <<-EOH
              php app/console #{env} oro:translation:dump en_US
              php app/console #{env} oro:translation:dump en
              php app/console #{env} oro:translation:dump fr_FR
              php app/console #{env} oro:translation:dump fr
          EOH
      end

      display_command do
        command "php bin/console assets:install #{env}"
      end
      run_command do
        command "php bin/console assets:install #{env} --ansi"
        user node[:app_user]
        group node[:app_group]
        environment node[:deploy][node[:app_name]][:environment_variables]
        cwd "#{release_path}/"
      end

    end
  end

  if node[:app][:is_installed] == 'true'

    display_command do
      command "php bin/console do:sc:up --force"
    end
    run_command do
      command 'php bin/console do:sc:up --force --ansi'
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end

    display_command do
      command "php bin/console cache:warmup #{env}"
    end
    run_command do
      command "php bin/console cache:warmup #{env} --ansi"
      user node[:app_user]
      group node[:app_group]
      environment node[:deploy][node[:app_name]][:environment_variables]
      cwd "#{release_path}/"
    end
  end
end

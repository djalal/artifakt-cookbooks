include_recipe 'apache2'

node[:mod_php5_apache2][:packages].each do |pkg|
  package pkg do
    action :install
    ignore_failure(pkg.to_s.match(/^php-pear-/) ? true : false) # some pear packages come from EPEL which is not always available
    retries 3
    retry_delay 5
  end
end

if node[:app][:language] == "73"
  bash "PHP73 fix for APCU and MEMCACHE" do
    code <<-EOH
      mv -f /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6 /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6.bkp
      yum reinstall -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm || yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
      mv -f /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6.bkp /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
      yum reinstall -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm || yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
      yum install -y php73-php-pecl-apcu php73-php-pecl-memcache --enablerepo remi-php73
      cp -n /opt/remi/php73/root/usr/lib64/php/modules/apcu.so /usr/lib64/php/7.3/modules/
      cp -n /opt/remi/php73/root/usr/lib64/php/modules/memcache.so /usr/lib64/php/7.3/modules/
      rm -rf /usr/lib64/php/7.3/modules/curl.so
      cp -n /opt/remi/php73/root/usr/lib64/php/modules/curl.so /usr/lib64/php/7.3/modules/
      bash -c "echo extension=apcu.so > /etc/php-7.3.d/10-apcu.ini"
      bash -c "echo extension=memcache.so > /etc/php-7.3.d/10-memcache.ini"
    EOH
  end
end

node[:deploy].each do |application, deploy|
  if deploy[:application_type] != 'php'
    Chef::Log.debug("Skipping deploy::php application #{application} as it is not an PHP app")
    next
  end
  next if node[:deploy][application][:database].nil?

  bash "Enable network database access for httpd" do
    boolean = "httpd_can_network_connect_db"
    user "root"
    code <<-EOH
      semanage boolean --modify #{boolean} --on
    EOH
    not_if { OpsWorks::ShellOut.shellout("/usr/sbin/getsebool #{boolean}") =~ /#{boolean}\s+-->\s+on\)/ }
    only_if { platform_family?("rhel") && ::File.exist?("/usr/sbin/getenforce") && OpsWorks::ShellOut.shellout("/usr/sbin/getenforce").strip == "Enforcing" }
  end

  case node[:deploy][application][:database][:type]
  when "postgresql"
    include_recipe 'mod_php5_apache2::postgresql_adapter'
  else # mysql or just backwards compatible
    include_recipe 'mod_php5_apache2::mysql_adapter'
  end
end

include_recipe 'apache2::mod_php5'

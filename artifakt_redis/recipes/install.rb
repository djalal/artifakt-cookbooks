#
# Cookbook Name: artifakt_redis
# Recipe: install
#

if  node[:ec][:installed] == "true"

    display_title do
        title 'Installation of redis'
    end

    bash "install_es_17" do
        user "root"
        code <<-EOH
        yum install epel-release
        yum update
        yum install -y redis
        /etc/init.d/redis start
        chkconfig --add redis
        EOH
    end
end

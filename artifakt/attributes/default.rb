#
# Cookbook Name: artifakt
# Attributes: default
#

default[:app][:maintenance] = 'false'

default[:ec][:installed] = false
default[:elc][:prefix] = 'magento_'
default[:php][:session_path] = '/tmp'
default[:app][:shared_files] = []
default[:command][:type] = 'none'
default[:app][:web_engine] = "apache"
default[:es][:installed] = "false"
default[:php][:prestissimo]=true
  
if ((node[:app][:type] == 'magento2' && node[:app][:version] == '2.4'))
    node.override[:app][:web_engine]="nginx"
    if node[:stack][:type] == 'standard' 
      node.override[:es][:installed]="true"
      node.override[:es][:version_es]="7.6.0"
      node.override[:db][:host]='127.0.0.1'
    else
      node.override[:es][:installed]="false"
    end
    node.override[:php][:prestissimo]=false
    node.override[:app][:language]="73"
    node.override[:app][:language_long]="7.3"
    node.override['php-fpm']['package_name']="php73-fpm"
    node.override['php-fpm']['service_name']="php-fpm"
end
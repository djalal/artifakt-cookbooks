#
# Cookbook Name: artifakt
# Recipe: deploy
#
display_text do
    text 'COOKBOOK - Artifakt / recipes / deploy.rb'
end

include_recipe 'deploy::php'

if node[:app][:web_engine] == 'nginx'
    display_title do
        title "Restart Nginx+PHP-FPM and reloading OpCache"
    end
    service "nginx" do
        action :restart
    end
    service "php-fpm" do
        action :restart
    end
else
    display_title do
        title "Restart Apache2 and reloading OpCache"
    end
    service "apache2" do
        action :restart
    end
end

if node[:quanta][:token]
    display_title do
        title 'Push deploy notification to Quanta'
    end

    include_recipe 'artifakt_quanta::deploy'
end

display_title do
    title 'Deploy succeeded'
end
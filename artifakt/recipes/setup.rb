#
# Cookbook Name: artifakt
# Recipe: setup
#
display_text do
  text 'COOKBOOK - Artifakt / recipes / setup.rb'
end

include_recipe 'artifakt_logs::install'
include_recipe 'artifakt_cloudwatchagent::setup'
if node[:stack][:isScalable] == 'true'
    include_recipe 'artifakt_efs::mount'
end

node.default[:command][:type] = 'setup'

display_title do
  title 'App type:'+node[:app][:type] == 'magento2'+' / App version: '+node[:app][:version]
end

if ((node[:app][:type] == 'magento2' && node[:app][:version] == '2.4'))
  display_title do
    title 'Magento 2, version 2.4 selected, set nginx, es 7.6.0, no prestissimo, language 7.3'
  end
  node.override[:app][:web_engine]="nginx"
  if node[:stack][:type] == 'standard' 
    node.override[:es][:installed]="true"
    node.override[:es][:version_es]="7.6.0"
    node.override[:db][:host]='127.0.0.1'
  else
    node.override[:es][:installed]="false"
  end
  node.override[:php][:prestissimo]=false
  node.override[:app][:language]="73"
  node.override[:app][:language_long]="7.3"
  node.override['php-fpm']['package_name']="php73-fpm"
  node.override['php-fpm']['service_name']="php-fpm"
end

include_recipe 'artifakt_docker::install'
include_recipe 'artifakt_htpasswd::setup'
if node[:app][:web_engine] == 'nginx'
  include_recipe 'php-fpm'
  include_recipe 'nginx'
  include_recipe 'mod_php5_apache2::mysql_adapter'
else
  include_recipe 'mod_php5_apache2'
end

include_recipe 'artifakt_elasticsearch::install'
include_recipe 'artifakt_redis::install'
include_recipe 'artifakt_crontab::install'
include_recipe 'artifakt_crontab::setup'
include_recipe 'ssh_users'
include_recipe 'artifakt_sftp_users'

if node[:quanta][:token]
  display_title do
    title 'Installation Quanta agent'
  end
  
  include_recipe 'artifakt_quanta::setup'
end

display_title do
  title 'Configuration of php.ini'
end

template "/etc/php.ini" do
    source "php.ini.erb"
    mode 0777
    group "root"
    owner "root"
end

if node[:email][:type] == 'no_send'
    package 'sendmail' do
      action :remove
    end
end

include_recipe 'artifakt_composer::install'
include_recipe "artifakt_app_#{node[:app][:type]}::setup"

#
# Cookbook Name: artifakt_varnish
# Recipe: install
#
template '/etc/yum.repos.d/varnish.repo' do
  source 'varnish.repo.erb'
end

package "jq" do
  retries 3
  retry_delay 5
end
package "pygpgme" do
  retries 3
  retry_delay 5
end
package "jemalloc" do
  retries 3
  retry_delay 5
end

package "nginx" do
  retries 3
  retry_delay 5
end

execute "Install Varnish 4.1" do
  command "yum -q makecache -y --disablerepo=amzn-updates --disablerepo=amzn-main --disablerepo=epel --enablerepo=varnishcache_varnish41"
  command "yum install varnish --disablerepo=amzn-updates --disablerepo=amzn-main --disablerepo=epel -y"
end

execute "chkconfig varnish and nginx" do
  command "chkconfig varnish on"
  command "chkconfig nginx on"
end

execute "Configure /etc/sysconfig/varnish" do
  command "sed -i.old -e 's/VARNISH_LISTEN_PORT\=6081/VARNISH_LISTEN_PORT\=80/' /etc/sysconfig/varnish"
end

template "/etc/varnish/default.vcl" do
    source "default.vcl.erb"
    owner 'root'
    group 'root'
    mode 0644
end

template "/etc/nginx/nginx.conf" do
    source "nginx.conf.erb"
    owner 'root'
    group 'root'
    mode 0644
end

execute "Restart service" do
    command "service varnish start"
    command "service nginx start"
end
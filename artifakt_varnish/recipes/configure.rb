#
# Cookbook Name: artifakt_varnish
# Recipe: configure
#
if node[:app][:type] == 'magento2'
    layer = "magento-layer"
else
    layer = "#{node[:app][:type]}-layer"
end

# call the aws api to fetch the active instance internal DNS names
instances = %x(aws --region=us-east-1 opsworks describe-instances --layer-id=#{node['opsworks']['layers'][layer]['id']} |jq '.Instances[]'.PrivateDns).gsub(/["]/, '').split("\n")

if(instances.count < 1)
    Chef::Log.error("No instances found. No changes made")
    return
end

if node[:stack][:type] != 'onsteroid'
    template "/etc/varnish/backends.vcl" do
        Chef::Log.debug("Activating these varnish backends:  [#{instances.join(' ')}]")
        source "backends.vcl.erb"
        owner 'root'
        group 'root'
        mode 0644
        variables(
            :instances => instances,
        )
    end
end

template "/etc/sysconfig/varnish" do
  Chef::Log.debug("Change varnish configuration")
  source "varnish.erb"
  owner 'root'
  group 'root'
  mode 0644
end

execute "start varnish deamon" do
    command "/etc/init.d/varnish start"
end

execute "reload vcl config" do
    command "/usr/sbin/varnish_reload_vcl"
end

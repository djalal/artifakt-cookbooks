#
# Cookbook Name: artifakt_app_magento
# Definition: artifakt_magento_deploy
#

define :artifakt_magento_deploy  do
  release_path = node[:app_release_path]

  execute "Remove local.xml is not installed" do
    cwd "#{release_path}/"
    command "rm -f app/etc/local.xml"
    only_if { node[:app][:is_installed] == 'false' }
  end


  display_title do
    title "Running custom flush cache command of magento 1"
  end
  display_command do
      command "n98-magerun cache:flush"
  end
  run_command do
      command 'n98-magerun cache:flush'
      user node[:app_user]
      group node[:app_group]
      cwd "#{release_path}/"
      ignore_failure true
  end

end

# v0.5.3
- Fix:
  - *_Actions_*
      * Fix rollback by remove old failed releases and check if folder releases exists
      * Fix redis-cli by removing the package (not used by the customer)

- Enhancements:
  - *_Actions_*:
      * Add rollback hook /artifakt/rollback.sh

# v0.5.2
- Fix:
  - *_Actions_*
      * Fix build failed when `node[:composer][:version]` is not set
      * Fix prestissimo

- Enhancements:
  - *_Actions_*:
      * Support Magento 2.4 version
      * Elasticsearch using docker now
      * Support mysql 8.0

# v0.5.1

- Enhancements:
  - *_Actions_*:
      * Add capability to specify the composer version using variables: composer : version

# v0.5.0

- Enhancements:
  - *_Actions_*:
      * Add ARTIFAKT_DEPLOYMENT_ID in environment variables
      * Use ARTIFAKT_DEPLOYMENT_ID for redis prefix
      * Add ARTIFAKT_IS_MAIN_INSTANCE in environment variables

# v0.4.12

- Enhancements:
  - *_Actions_*:
      * Show GIT Hash in deploy log
      * Sort environment variables
      * Add cloudwatch agent installation

# v0.4.11

- Fix:
  - *_Actions_*
      * Mount script modified for EFS, mount command used and information wrote in the FSTAB to keep the "/mnt/shared" folder available after reboot and re-setup
      * For Magento 2, absolute_code_root added to cwd command (for custom configuration) for maintenance scripts (on and off)
      * For Magento 2, stop flushing cache before deploy (failed for new install)

- Enhancements:
  - *_Actions_*:
      * For Magento 1, clear redis cache during deploy
      * For Magento 1, clear redis cache for clear-cache job


# v0.4.10

- Enhancements:
  - *_Actions_*:
      * New recipe artifakt::resize-disk for extend file system after a size modification on EBS
      * Add some default artifakt environment variables (ARTIFAKT_MYSQL_HOST, ARTIFAKT_ENVIRONMENT_NAME, etc)
      * Add capabitity to use custom environments variables during deploy and setup hooks

# v0.4.9

- Fix:
  - *_Actions_*
      * Fix SFTP Access

# v0.4.8

- Fix:
  - *_Actions_*
      * Fix streaming multiple files in one log stream name on CloudwatchLogs does NOT work
      * Fix Magento maintenance.flag path (code root)
- Enhancements:
  - *_Actions_*:
      * Each logs files has its own log stream name on CloudwatchLogs
      * Hide secured environment variable in logs

# v0.4.7

- Fix:
  - *_Actions_*
      * Change SFTP user configuration
      * Disable mysql log general
      * Show True Client IP apache2 and nginx

- Enhancements:
  - *_Actions_*
      * Set Keep release to 3

# v0.4.6

- Fix:
  - *_Actions_*
      * Fix remove app_dev.php for prod
      * Fix health_check nginx for Magento 2
      * Fix restart mysql starter after crashed
      * Fix install supervisor with pip
      * Fix nginx permission logrotate
      * Fix code root nginx Magento 1

- Enhancements:
  - *_Actions_*
      * Empty cache Magento before maintenance
      * Add custom Hook for setup
      * Customize nginx port for magento 2 and varnish
      * Add Environnment variables to nginx
      * Realtime logs for jobs
      * Add Docker package
      * Improve buffer Nginx

# v0.4.5

- Fix:
  - *_Actions_*
      * MySql installation with Nginx
      * Clear Cache Magento with right permission
      * Clear Cache Magento with command cache:flush instead of cache:clean

- Enhancements:
  - *_Actions_*
      * Security Nginx Magento 2 pub/.user.ini
      * Add PHP7.3 and fixing for APCU & Memcache
      * Remove EBS recipes to use new instance types (m5, t3)

# v0.4.4

- Fix:
  - *_Actions_*
      * Quanta agent installation

- Enhancements:
  - *_Actions_*
      * Improve Magento Nginx files
      * Monitor supervisord and mysqld process
      * Improve Apache security

# v0.4.3

- Fix:
  - *_Actions_*
      * Forwarded IP from ELB for nginx

- Enhancements:
  - *_Actions_*
      * Share root files between releases.
      * Install Quanta agento and push deploy notification if token is set
      * Remove server Header Apache

# v0.4.2

- Fix:
  - *_Actions_*
      * Fix buildpack Drupal v7.6
      * Fix buildpack Akeneo (all versions)
      * Fix environment variables not set when running a command with function `run_command`

- Enhancements:
  - *_Actions_*
      * Installation of good version of mysql sent by OpsWork
      * Creation of local database with good charset and collation
      * mysql command and mysqldump are now available for nginx mode
      * Install supervisor for every servers

# v0.4.1

- Fix:
  - *_Actions_*
      * Fix permission of folder `/var/log/mysql` for `mysql` be able to write logs in it
      * Move mount directory at the end of the deploy and remove var shared directory for Magento 2
      * Fix php-fpm PID

- Enhancements:
  - *_Actions_*
      * Improve clear-cache recipe with Magento 2 custom command and Reload web Engine + OpCache
      * Add Reload web Engine + OpCache at the end of the deploy
      * Install Htop


# v0.4.0

- Fix:
  - *_Actions_*:
      * Buildpack PHP
      * Buildpack Symfony
      * Buildpack Drupal (version 8.7)
      * Buildpack Wordpress
      * Buildpack Magento

- Enhancements:
  - *_Architectures_*:
      * Deletion of recipes apps: oro, joomla, marello, orocommerce, prestashop, satis and typo
      * Deletion of recipe artifakt_app_nginxconfig
      * Deletion of `/cookbooks/before_configure` and `/cookbooks/after_setup`

  - *_Actions_*:
      * New way of handle symfony version and symfony based app
      * Display usefull logs for user and output of commands (ex: `composer install` or title like "---> Gettings code")
      * Reconfiguration of cloud watch agent for send good logs files

display_text do
  text 'COOKBOOK - deploy / recipes / default.rb'
end

if ((node[:app][:type] == 'magento2' && node[:app][:version] == '2.4'))
  display_title do
    title 'Magento 2, version 2.4 selected, set nginx, es 7.6.0, no prestissimo, language 7.3'
  end
  node.override[:app][:web_engine]="nginx"
  if node[:stack][:type] == 'standard' 
    node.override[:es][:installed]="true"
    node.override[:es][:version_es]="7.6.0"
    node.override[:db][:host]='127.0.0.1'
  else
    node.override[:es][:installed]="false"
  end
  node.override[:php][:prestissimo]=false
  node.override[:app][:language]="73"
  node.override[:app][:language_long]="7.3"
  node.override['php-fpm']['package_name']="php73-fpm"
  node.override['php-fpm']['service_name']="php-fpm"
end

include_recipe 'dependencies'

node[:deploy].each do |application, deploy|

  opsworks_deploy_user do
    deploy_data deploy
  end

end

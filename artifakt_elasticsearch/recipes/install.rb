#
# Cookbook Name: artifakt_elasticsearch
# Recipe: install
#

if node[:es][:installed] == "true"
  bash "install_es" do
    user "root"
    code <<-EOH
    mkdir -p /mnt/shared/elasticsearch
    chmod -R 777 /mnt/shared/elasticsearch
    sysctl -w vm.max_map_count=262144
    docker stop elasticsearch
    docker rm elasticsearch
    docker run -d -p 9200:9200 -p 9300:9300 \
    --name elasticsearch \
    -e cluster.name=es-docker-cluster \
    -e node.name=es01 \
    -e "ES_JAVA_OPTS=-Xms512m -Xmx512m" \
    -e bootstrap.memory_lock=true \
    -e "discovery.type=single-node" \
    --volume=/mnt/shared/elasticsearch:/usr/share/elasticsearch/data \
    docker.elastic.co/elasticsearch/elasticsearch:#{node[:es][:version_es]}
    EOH
  end
end

if (node[:app][:type] == 'magento2' && node[:app][:version] == '2.4')
  display_text do
    text "Elasticsearch - Magento 2, version 2.4 found detected"
  end
  if node[:stack][:type] != 'standard' 
    display_text do
      text "Elasticsearch - Stack type: "+node[:stack][:type]
    end
    if (node[:app][:aws_es])
      display_text do
        text "Elasticsearch - SUCCESS - Info aws_es found in app"
      end
      if ((node[:app][:aws_es][:endpoint] && (node[:app][:aws_es][:username])  && (node[:app][:aws_es][:password])))
        display_text do
          text "Elasticsearch - SUCCESS - Endpoint, username and password found in app, set env vars."
        end
        display_title do
          title 'Set AWS Elasticsearch proxy'
        end
        bash "Set AWS Elasticsearch proxy" do
          user "root"
          code <<-EOH
          if [ ! -f "/etc/nginx/sites-enable/esAws" ]
          then
          echo 'Create esAws vhost on port 8090'
          echo "server {" > /etc/nginx/sites-available/esAws
          echo "  listen 8090;" >> /etc/nginx/sites-available/esAws
          echo "  location / {" >> /etc/nginx/sites-available/esAws
          echo "      proxy_pass #{node[:app][:aws_es][:endpoint]};" >> /etc/nginx/sites-available/esAws
          echo "  }" >> /etc/nginx/sites-available/esAws
          echo "}" >> /etc/nginx/sites-available/esAws
          ln -s /etc/nginx/sites-available/esAws /etc/nginx/sites-enabled/esAws
          service nginx reload
          echo "The vhost has been created, please use these information to connect to your ES with CLI: --search-engine=elasticsearch7 --elasticsearch-host=localhost --elasticsearch-username=<username> --elasticsearch-port=8090 --elasticsearch-password=<password> --elasticsearch-enable-auth=1"
          else
            echo "The esAws vhost already exists. You can use these information to connect to your ES with CLI: --search-engine=elasticsearch7 --elasticsearch-host=localhost --elasticsearch-username=<username> --elasticsearch-port=8090 --elasticsearch-password=<password> --elasticsearch-enable-auth=1"
          fi
          EOH
        end
      else
        display_text do
          text 'Please indicate all information for aws_es: endpoint (https://xxxx), username and password.'
        end
      end
    else
      display_text do
        text "Elasticsearch - ERROR - Info aws_es not set in app"
      end
    end 
  end
end